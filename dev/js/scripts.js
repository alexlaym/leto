$(document).ready(function () {

	const lazyLoadInstance = new LazyLoad({
		elements_selector: '.lazy',
		load_delay: 100
	});

	prizeImgReplace();

	AOS.init({
		duration: 1000
	});

	const qaBlocks = $('.main-qa__block')
	qaBlocks.each(function () {
		$(this).attr('data-aos-delay', `${100 * $(this).index()}`)
	})

	if ($(window).width() < 451) {
		const mainBanner = $('.main-banner')
		mainBanner.each(function () {
			$(this).attr('data-bg', `url(img/content/main-banner-background-mob.png)`)
		})
	}

	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});

	$('body').on("click", '.js-popup-ajax', function (e) {
		e.preventDefault()
		let image_link = $(this).attr('href')

		$.magnificPopup.open({
			items: {
				src: image_link,
			},
			removalDelay: 500,
			mainClass: 'mfp-fade',
			type: 'ajax',
		})
	})
	$('.js-popup-gallery').each(function () {
		$(this).magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: {
				enabled: true
			},
			zoom: {
				enabled: false
			}
		});
	})
	$(window).on('resize', prizeImgReplace)

	$(document).on('click', '.js-ac', function (e) {
		if (e.target.hasAttribute("href")) {
			e.preventDefault
		}
		else {
			$(this).toggleClass('active').find('.js-ac-dd').slideToggle(300)
			$(this).siblings('.js-ac').removeClass('active').find('.js-ac-dd').slideUp(300)
		}
	})

	$('body').on("click", '.js-popup-ajax', function (e) {
		e.preventDefault()
		let image_link = $(this).attr('href')

		$.magnificPopup.open({
			items: {
				src: image_link,
			},
			removalDelay: 500,
			mainClass: 'mfp-fade',
			type: 'ajax',
		})
	})

	function prizeImgReplace() {
		let item = $('.main-products__prizes-item--w100')

		item.each(function () {
			const width = $(window).width(),
				thisItem = $(this),
				image = $(this).find('.main-products__prizes-img'),
				label = $(this).find('.main-products__prizes-label');

			width <= 600 ? image.insertAfter(label) : thisItem.append(image)
		})
	}
})

